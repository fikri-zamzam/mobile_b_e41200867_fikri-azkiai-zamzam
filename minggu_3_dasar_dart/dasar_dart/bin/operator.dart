void main() {
  // Operator Aritmatika
  print("Operator Aritmatika\n");
  var a = 12;
  var b = 22;
  print(a + b);
  print(a - b);
  print(a * b);
  print(a / b);
  print(a % b);

  print("-------------\n");

  //
  // Operator Perbandingan

  print("Operator Perbandingan\n");
  var angka = 87;
  var mobil = "mercedes";
  print(angka == 100); //false
  print(angka == 87); //true
  print(mobil != "toyota"); // true
  print(angka > 100); // false
  print(angka < 100); // true
  print("-------------\n");

  //
  //operator Kondisional
  print("Operator Kondisional\n");
//   OR ( || )
  print("OR OPS\n");
  print(true || true); // true
  print(true || false); // true
  print(true || false || false); // true
  print(false || false); // false
//  AND ( && )
  print("AND OPS\n");
  print(true && true); // true
  print(true && false); // false
  print(false && false); // false
  print(false && true && true); // false
  print(true && true && true); // true
}
