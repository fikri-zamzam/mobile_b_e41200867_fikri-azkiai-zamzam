void main(List<String> args) {
  var nama = "John";
  var peran = "Werewolf";

  if (nama == "") {
    print("Nama harus diisi !");
  } else if (peran == "") {
    print("Pilih Peranmu untuk memulai game");
  } else if (peran == "Penyihir") {
    print("Selamat datang di Dunia Werewolf, $nama");
    print(
        "Halo Penyihir $nama, kamu dapat melihat siapa yang menjadi werewolf!");
  } else if (peran == "Guard") {
    print("Selamat datang di Dunia Werewolf, $nama");
    print(
        "Halo Guard $nama, kamu akan membantu melindungi temanmu dari serangan werewolf.");
  } else if (peran == "Werewolf") {
    print("Selamat datang di Dunia Werewolf, $nama");
    print("Halo Werewolf $nama, Kamu akan memakan mangsa setiap malam!");
  }
}
