void main() {
  print("Perulangan While\n");

  var ulang = 9;
  var awal = 1;
  while (awal <= ulang) {
    print("ini nilai ke-$awal");
    awal++;
  }

  print("\n");
  print("Perulangan For\n");
  //perulangan kali 2
  for (var i = 1; i < 16; i += 2) {
    print("Perulangan ganda ke-$i");
  }
}
