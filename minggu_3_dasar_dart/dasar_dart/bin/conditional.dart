void main() {
  var stok = 30;
  var status = "Tersedia";

  if (stok <= 0) {
    status = "kosong";
  }
  print("$status");
  print("\n");

  //
  //contoh kedua
  print("Kasus kedua");
  var saldo = 10000;
  var harga = 21000;
  var gunakanpaylater = false;
  if (saldo < harga) {
    if (gunakanpaylater) {
      print("Tagihan anda akan dialihkan ke paylater\n");
    } else {
      print("Saldo anda tidak mencukupi\n");
    }
  } else if (saldo >= harga) {
    print("Selamat pesanan anda berhasil diproses\n");
  }

  //
//Switch Case
  print("Switch case\n");
  var menu = 1;
  switch (menu) {
    case 1:
      print("Cek Saldo\n");
      break;
    case 2:
      print("Transfer Bank\n");
      break;
    case 3:
      print("Tarik Tunai\n");
      break;

    default:
      print("Mohon masukkan angka yang benar\n");
  }
}
